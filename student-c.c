#include "student-c.h"

int isPrime(int digit)
{
    if(digit<2)
    {
        return 0;
    }

    for(int i=2 ; i*i<=digit ; i++)
    {
        if(digit%i==0)
        {
            return 0;
        }
    }
    return 1;
}

int average(int digits[ ], int size)
{
    int srednia=0;
    for(int n=0 ; n < size  ; n++)
    {
        srednia += digits[n];
    }

    srednia = srednia / size;

    return srednia;
}
