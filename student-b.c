#include "student-b.h"

int isOdd(int digit)
{
     if(digit%2!=0)
     {
         return 1;
     }
     else return 0;
}

int min(int digits[ ], int size)
{
    digits[size]={1,3,4,23,10,8};
    int min=digits[0];
    for(int i=0;i<size;i++)
    {
       if(digits[i]<min)
       {
           min=digits[i];
       }
    }
    return min;
}
