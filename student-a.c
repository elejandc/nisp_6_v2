#include "student-a.h"

int isEven(int digit)
{
    if(digit % 2 == 0) return 1;
    else return 0;
}

int max(int digits[], int size)
{
    int max_index = 0;
    for(int i = 0; i < size; i++)
    {
        if(digits[i] > digits[max_index])
        {
            max_index = i;
        }
    }
    return max_index;
}
