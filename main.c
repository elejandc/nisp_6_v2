#include <stdio.h>
#include "student-a.h"
#include "student-b.h"
#include "student-c.h"
#include "student-d.h"

#define size 6

int main()
{
    int digit = 8;
    int digits[size] = {1, 2, 3, 4, 5, 6};

    //student a - Jan Czaja
    int isEvenResult = isEven(digit);
    int maxResult = max(digits, size);
    printf("Result isEven = %d\n", isEvenResult);
    printf("Result max = %d\n\n", maxResult);

    //student b - Michał Ciurej
    int isOddResult = isOdd(digit);
    int isMinResult = min(digits, size);
    printf("Result odd = %d\n", isOddResult);
    printf("Result min = %d\n\n",isMinResult);

    // student-c - Kamil Dereń
    int isPrimeResult = isPrime(digit);
    int averageResult = average(digits, size);
    printf("Result isPrime = %d\n", isPrimeResult);
    printf("Result average = %d\n\n", averageResult);

    // student-d - Jakub Dojka
    int isPositiveResult = isPositive(digit);
    int countEvenResult = countEven(digits, size);
    printf("Result isPositive = %d\n", isPositiveResult);
    printf("Result countEven = %d\n\n", countEvenResult);

    return 0;
}
